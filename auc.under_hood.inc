<?php

/**
 * @file
 * Functions & constants.
 */

const AUC_VIDEO_FORMATS = [
  'webm',
  'mpg',
  'mp2',
  'mpeg',
  'mpe',
  'mpv',
  'ogg',
  'mp4',
  'm4p',
  'm4v',
  'avi',
  'wmv',
  'mov',
  'qt',
  'flv',
  'swf',
  'avchd',
];

/**
 * See if imagemagick supports this file extension.
 *
 * @param $path_parts
 * @param &$imagemagick_convert_checked
 *
 * @return false|true
 */
function auc_ext_check($path_parts, &$imagemagick_convert_checked) {
  if (empty($path_parts['extension'])) {
    return FALSE;
  }

  // See if imagemagick supports this file extension.
  // Move to admin section eventually.
  if (!empty($imagemagick_convert_checked[$path_parts['extension']])) {
    return $imagemagick_convert_checked[$path_parts['extension']];
  }

  $result = _imagemagick_convert_exec('-version', $output);
  $file_copies = array();
  if (strpos($output, $path_parts['extension']) === FALSE) {
    // Make a copy of the failed upload for further review.
    if (!empty($_FILES['files']['name'])) {
      foreach ($_FILES['files']['name'] as $field_name => $value) {
        $file_name = $_FILES['files']['name'][$field_name];
        $file_location = $_FILES['files']['tmp_name'][$field_name];
        if (!is_uploaded_file($file_location)) {
          continue;
        }
//         $file_copies[] = file_unmanaged_copy($file_location, "public://debug/{$file_name}");
      }
    }
    if (!empty($_FILES['file']['name'])) {
      $file_name = $_FILES['file']['name'];
      $file_location = $_FILES['file']['tmp_name'];
      if (is_uploaded_file($file_location)) {
//         $file_copies[] = file_unmanaged_copy($file_location, "public://debug/{$file_name}");
      }
    }

    // Report Failure.
    watchdog('auc', 'auto upload convert failed. convert binary does not support the @ext filetype.<br><pre>@result</pre><br>@output<br><pre>@file_copies</pre>', array(
      '@ext' => $path_parts['extension'],
      '@result' => var_export($result, TRUE),
      '@output' => $output,
      '@file_copies' => print_r($file_copies, TRUE),
    ));
    $imagemagick_convert_checked[$path_parts['extension']] = FALSE;
  }
  else {
    $imagemagick_convert_checked[$path_parts['extension']] = TRUE;
  }

  return $imagemagick_convert_checked[$path_parts['extension']];
}

/**
 * Edit image file - convert or optimize, depend on extension and command.
 *
 * @param $command
 *   Command with file name, options etc.
 * @param $program
 *   Which program to run: imagemagick or exec (darktable, ffmpeg, ghostscript).
 *
 * @return bool
 */
function auc_convert_extension($command, $program): bool {

  $result = $output = $error = NULL;

  switch ($program) {
    case 'imagemagick':
      $result = _imagemagick_convert_exec($command, $output, $error);
      break;
    case 'exec';
      // ffmpeg, darktable, ghostscript require extension for output file!
      // It is impossible to convert into the same tmp file in $_FILES
      // So we are creating new file with explicit extension and then replace
      // original uploading file with it.
      $result = auc_get_jpg_from_exec($command, $output, $error);
      break;
    case 'fast-rename':
      $result = TRUE; // Renaming will be in any way in auc_process_wrapper().
      break;
    default:
      break;
  }

  $no_error_msg = t('No error message.');
  if ($result !== TRUE || ($no_error_msg !== $error && !empty($error))) {
    // Report Failure.
    watchdog('auc', 'auto upload convert failed.<br>@command<br><pre>@result</pre><br>@output<br>@error', array(
      '@command' => $command,
      '@result' => var_export($result, TRUE),
      '@output' => $output,
      '@error' => $error,
    ));
    return FALSE;
  }
  else {
    return TRUE;
  }
}


/**
 * Validate image info on upload to prevent image errors on use.
 *
 * 1). Check that 'bits' < 50 and 'channels' < 5. If not then image is corrupt
 * most likely.
 * 2). Compare file extension and it`s mime type. They cannot mismatch!
 *
 * This validation run for images with follow mime types:
 * 'image/jpeg', 'image/gif', 'image/png', 'image/webp'.
 *
 * Also, we try to repair image with imagemagick.
 *
 * @param \stdClass $file
 *   Drupal file object.
 *
 * @return bool
 *   TRUE on success, FALSE otherwise.
 */
function auc_is_image_correct(stdClass $file): bool {
  $is_correct = TRUE;

  $file_path = drupal_realpath($file->uri);
  // Wait 2 seconds for the file to show up in the file system.
  if (!file_exists($file_path)) {
    sleep(1);
  }
  if (!file_exists($file_path)) {
    sleep(1);
  }

  // Suppress errors from getimagesize.
  $image_info = @getimagesize($file_path);

  // Get original extension
  $original_extension = '';
  if (!empty($file->source) && !empty($_FILES['files']['name'][$file->source])) {
    $original_extension = pathinfo($_FILES['files']['name'][$file->source], PATHINFO_EXTENSION);
  }
  elseif (!empty($file->filename)) {
    $original_extension = pathinfo($file->filename, PATHINFO_EXTENSION);
  }

  // Eventually have configuration for this.
  $conversion_lut = array(
    'image/png' => array(
      'imagemagick_command' => "%fn% 'png:%fn%'",
      'original_extension' => 'png',
    ),
    'image/jpeg' => array(
      'imagemagick_command' => "%fn% 'jpg:%fn%'",
      'original_extension' => 'jpg',
    ),
    'image/gif' => array(
      'imagemagick_command' => "%fn% 'gif:%fn%'",
      'original_extension' => 'gif',
    ),
    // 'image/webp' => array( todo broken webp not repairing such way, need to find other
    //   'imagemagick_command' => "%fn% 'webp:%fn%'",
    //   'original_extension' => 'webp',
    // ),
  );

  // Run through imagemagick convert as this might fix a bad image file.
  $imagemagick_fix = FALSE;
  if (!file_exists($file_path)
    || @exif_imagetype($file_path) === FALSE
    || (isset($image_info['channels']) && $image_info['channels'] >= 5)
    || (isset($image_info['bits']) && $image_info['bits'] >= 50)
    || auc_compare_extension_and_mime($file_path, $original_extension, $conversion_lut) === FALSE
  ) {
    if (!empty($file->filemime) && !empty($conversion_lut[$file->filemime]['imagemagick_command'])) {
      $command = str_replace('%fn%', $file_path, $conversion_lut[$file->filemime]['imagemagick_command']);
      auc_convert_extension($command, 'imagemagick');
      $image_info = @getimagesize($file_path);

      if (!file_exists($file_path)
        || @exif_imagetype($file_path) === FALSE
        || (isset($image_info['channels']) && $image_info['channels'] >= 5)
        || (isset($image_info['bits']) && $image_info['bits'] >= 50)
      ) {
        // Conversion failed.
        $is_correct = FALSE;
      }
      else {
        // Conversion worked.
        $is_correct = TRUE;
        $imagemagick_fix = TRUE;
      }
    }
    else {
      $is_correct = FALSE;
    }
  }

  // Log.
  $placeholders = array(
    '%filename' => $file->filename,
    '%filepath' => $file_path,
    '@file' => print_r($file, TRUE),
    '@post' => print_r($_POST, TRUE),
    '@files' => print_r($_FILES, TRUE),
    '@phpinput' => file_get_contents('php://input'),
    '@server' => print_r($_SERVER, TRUE),
  );
  $placeholders['%channels'] = !empty($image_info['channels']) ? $image_info['channels'] : 'not defined';
  $placeholders['%bits'] = !empty($image_info['bits']) ? $image_info['bits'] : 'not defined';

  if (!$is_correct) {
    $file_copy = '';
//     $file_copy = file_unmanaged_copy($file->uri, "public://debug/{$file->filename}");
    $placeholders['@file_copy'] = print_r($file_copy, TRUE);
    $message = 'Seems this image is broken. Filename: %filename, filepath: %filepath, channels: %channels, bits: %bits. <br><pre>@file</pre> <br><pre>@post</pre> <br><pre>@files</pre> <br><pre>@phpinput</pre> <br><pre>@server</pre> <br><pre>@file_copy</pre>';
    watchdog('auc', $message, $placeholders, WATCHDOG_WARNING);
  }
  if ($imagemagick_fix) {
    $message = 'This image was broken. imagemagick convert was able to fix it. Filename: %filename, filepath: %filepath, channels: %channels, bits: %bits. <br><pre>@file</pre> <br><pre>@post</pre> <br><pre>@files</pre> <br><pre>@phpinput</pre> <br><pre>@server</pre>';
    watchdog('auc', $message, $placeholders, WATCHDOG_INFO);
  }

  // Return status.
  return $is_correct;
}

/**
 * Get jpg image from raw image or video file.
 *
 * @param $command
 *   The shell command.
 * @param null $output
 *   Output from a shell command. (variable with reference)
 * @param null $error
 *   Was a shell command finished with error? (variable with reference)
 *
 * @return bool
 *   Plus variables $output and $error are changing by reference.
 */
function auc_get_jpg_from_exec($command, &$output = NULL, &$error = NULL): bool {
  $is_success = FALSE;

  $result = exec("{$command}", $output, $error);

  if ($result !== FALSE && $error == 0) {
    $is_success = TRUE;
  }

  return $is_success;
}

/**
 * Get list allowed filetypes and extensions for this Drupal field.
 *
 * @param string $field_name example: 'field_upload_file_und_2'
 *
 * @return array
 */
function auc_get_field_allowed_extensions(string $field_name): array {
  $allowed_extensions = [];

  $all_fields_instances = field_info_instances();

  foreach ($all_fields_instances as $entity_field_instance) {
    foreach ($entity_field_instance as $content_type_field_instance) {
      foreach ($content_type_field_instance as $field_instance) {
        // Search for necessary field name.
        if (
          strpos($field_name, $field_instance['field_name']) !== FALSE
          && !empty($field_instance['settings']['file_extensions'])
        ) {
          // Convert to array allowed extensions.
          $allowed_extensions = explode(' ', $field_instance['settings']['file_extensions']);
        }
      }
    }
  }

  return $allowed_extensions;
}

/**
 * Create command for this file, run program and update $_FILES if success.
 *
 * @param array $conversion_lut
 *   Array with all supported image types with this module extensions and
 *   commands for it.
 *
 * @param array $path_parts
 *   Array with file information like basename, extension etc.
 *
 * @param string $field_name
 */
function auc_process_wrapper($conversion_lut, $path_parts, string $field_name = '') {

  // Build command for external program.
  $tmp_name = !empty($field_name) ? $_FILES['files']['tmp_name'][$field_name] : $_FILES['file']['tmp_name'];
  $input_file_extension = $path_parts['extension'];
  $output_file_extension = $conversion_lut[$input_file_extension]['extension'];

  $program = $conversion_lut[$input_file_extension]['program'];
  $command = $conversion_lut[$input_file_extension]['command'];

  // Add in file rename if not using imagemagick
  if ($program !== 'imagemagick') {
    $command = "{$command} && rm %fn% && mv %fn%.jpg %fn%";
  }
  // Replace %fn% token with the actual filename.
  $command = str_replace('%fn%', escapeshellarg($tmp_name), $command);

  // See if imagemagick supports this file extension.
  // Move to admin section eventually.
  $imagemagick_convert_checked = array();
  // if ($program === 'imagemagick' && !auc_ext_check($path_parts, $imagemagick_convert_checked)) {
  //   return;
  // } fixme this is not necessary already. Commented now. Will be removed.

  // Check that field allows images but not video - otherwise fast return.
  if ($input_file_extension === 'mp4') {
    $allowed_extensions = auc_get_field_allowed_extensions($field_name);
    if (
      !is_array($allowed_extensions)
      || (array_search('jpeg', $allowed_extensions) === FALSE
        && array_search('jpg', $allowed_extensions) === FALSE)
      || count(array_intersect(AUC_VIDEO_FORMATS, $allowed_extensions)) >= 1 // for test use '== 0'
    ) {
      return;
    }
  }

  // Do conversion in place.
  if (auc_convert_extension($command, $program)) {

    // We need to update in the super global array $_FILES next things:
    // 'name', 'type', 'size'.
    if (!empty($field_name)) {
      $file_name_ref = &$_FILES['files']['name'][$field_name];
      $file_type_ref = &$_FILES['files']['type'][$field_name];
      $file_size_ref = &$_FILES['files']['size'][$field_name];
    }
    else {
      $file_name_ref = &$_FILES['file']['name'];
      $file_type_ref = &$_FILES['file']['type'];
      $file_size_ref = &$_FILES['file']['size'];
    }

    // Update file extension.
    $file_name_ref = "{$path_parts['filename']}.{$output_file_extension}";

    // Update 'type' when it is necessary.
    if (!empty($conversion_lut[$input_file_extension]['output_mime_type'])) {
      $file_type_ref = $conversion_lut[$input_file_extension]['output_mime_type'];
    }

    // Update 'filesize'.
    $filesize = filesize($tmp_name);
    if ($filesize > 16) {
      $file_size_ref = $filesize;
    }

    // Report success.
    watchdog('auc', 'auto upload convert worked. <br>@command<br> <pre>@debug</pre>', array(
      '@command' => $command,
      '@debug' => print_r([$field_name, $input_file_extension, $path_parts, $conversion_lut[$input_file_extension]], TRUE),
    ));
  }
}

/**
 * Get mime type.
 *
 * @param string $file_path
 *   Path to file.
 *
 * @return false|mixed|string|null
 */
function auc_get_mime_type(string $file_path) {

  $type = NULL;

  if (function_exists('finfo_file')) {
    $file_info = finfo_open(FILEINFO_MIME_TYPE);
    $type = finfo_file($file_info, $file_path);
    finfo_close($file_info);
  }

  if (
    $type === FALSE
    || $type === 'application/octet-stream'
    || $type === 'text/plain')
  {
    $second_try = exec('file -b --mime-type ' . escapeshellarg($file_path), $output, $returned_code);
    if ($returned_code === 0 && $second_try) {
      $type = $second_try;
    }
  }

  return $type;
}

/**
 * Compare mime type of file with its extension.
 *
 * @param string $file_path
 * @param string $original_extension
 * @param array $conversion_lut todo create one array for all functions
 *
 * @return bool
 *   True - when file extension and mime type match, false otherwise.
 */
function auc_compare_extension_and_mime(string $file_path, string $original_extension, array $conversion_lut):bool {
  $is_match = FALSE;

  $type = auc_get_mime_type($file_path);

  if (
    !empty($type)
    && is_string($type)
    && isset($conversion_lut[$type])
    && $original_extension === $conversion_lut[$type]['original_extension'])
  {
    $is_match = TRUE;
  }
  return $is_match;
}
