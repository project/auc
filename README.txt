Module 'Auto Upload Convert'.

DESCRIPTION
-----------
Module converts images to jpeg format during file uploading from these formats:
- 'heic' (Apple image format);
- 'psd' (Photoshop image format);
- 'ai';
- 'eps';
- 'cr2' (preview from raw image);
- 'mp4' (preview slide from video);
- 'pdf';
- 'jfif'.


Also, it adds validation for all uploading web-images: jpeg, gif, png, webp.
It checks that 'bits' < 50 and 'channels' < 5.
If not then image is most likely broken.

Works with all Imagemagick formats (about 150) with standard settings for jpeg:
"optimize -quality 85%".

REQUIREMENTS
------------
ImageMagick:
https://www.drupal.org/project/imagemagick

Darktable (darktable-cli):
https://www.darktable.org/install/

FFmpeg:
https://www.ffmpeg.org/

Ghostscript
-----------
https://www.ghostscript.com/

INSTALLATION
------------
Simple activate it on '/admin/modules' page.
